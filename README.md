# myapp_test

A new Flutter project. je modifie un fichier dans une seule branche

## Installation des dependance sur un projet Flutter

- ouvrez un cmd dans le dossier à la racine (où il y a le `pubspec.yml` de votre projet Flutter)
- `flutter pub add firebase_core`
- `flutter pub add firebase_auth`
- `flutter pub add cloud_firestore`
- `flutter pub add provider`
- installation du client [Firebase CLI](https://firebase.google.com/docs/cli)
- `dart pub global activate flutterfire_cli`
- `flutterfire configure`

C'est bon vous avez installé Firebase dans votre projet Flutter !

**sources:** [https://firebase.google.com/codelabs/firebase-get-to-know-flutter#3](https://firebase.google.com/codelabs/firebase-get-to-know-flutter#3)

## Getting Started with Flutter

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


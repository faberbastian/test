import 'package:flutter/material.dart';
import 'package:myapp_test/classes/bdd.dart';
import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/utils.dart';
import 'package:myapp_test/components/appbar_custom.dart';
import 'package:myapp_test/components/bulle.dart';
import 'package:myapp_test/components/small_button.dart';

class ParameterPage extends StatefulWidget {
  const ParameterPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ParameterPageState();
  }
}

class ParameterPageState extends State<ParameterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: const Text("Paramètres"),
        actionButton: const Text(""),
      ),
      body: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Bulle(
            message: getFirstsInitiales(currentUser.surnom),
            size: 150,
            circle: true,
            fontSize: 45,
            bg: currentUser.couleur,
            subtext: currentUser.surnom,
          ),
        ),
        SmallButton(
          onPressed: () {
            /*
            A changer par deconnexion user
            */
            currentUser = User(
                surnom: "Utilisateur default", couleur: Colors.green.shade300);

            Navigator.pushNamedAndRemoveUntil(
                context, "/connexion", (r) => false);
          },
          message: "se déconnecter",
          mainColor: const Color.fromRGBO(255, 111, 111, 0.9),
        )
      ]),
    );
  }
}

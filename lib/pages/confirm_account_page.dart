import 'package:flutter/material.dart';
import 'package:myapp_test/classes/bdd.dart';
import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/utils.dart';
import 'package:myapp_test/components/appbar_custom.dart';
import 'package:myapp_test/components/bulle.dart';
import 'package:myapp_test/components/colorful_bubble.dart';
import 'package:myapp_test/components/footer_bottom_buttons.dart';
import 'package:myapp_test/pages/create_account_page.dart';

class ConfirmAccountPage extends StatefulWidget {
  const ConfirmAccountPage({Key? key, required this.user}) : super(key: key);

  final User user;

  @override
  State<StatefulWidget> createState() {
    return ConfirmAccountPageState();
  }
}

class ConfirmAccountPageState extends State<ConfirmAccountPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: const Text(
          "créer un nouveau compte",
          style: TextStyle(fontSize: 17.0),
        ),
        hasActionButton: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
                child: Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Bulle(
                message: getFirstsInitiales(widget.user.surnom),
                bg: widget.user.couleur,
                circle: true,
                size: 150,
                fontSize: 45,
                subtext: widget.user.surnom,
              ),
            ))
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
      bottomNavigationBar: FooterBottomButtons(
        onPressedNext: () {
          /*
          A changer avec l'enregistrement dans la bd
          */
          currentUser = widget.user;

          Navigator.pushNamedAndRemoveUntil(
              context, "/equipages", (r) => false);
        },
        onPressedPrec: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

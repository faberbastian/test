import "package:flutter/material.dart";
import 'package:myapp_test/classes/crew_user.dart';
import 'package:myapp_test/classes/equipage.dart';
import 'package:myapp_test/components/appbar_custom.dart';

class ParameterEquipagePage extends StatefulWidget {
  ParameterEquipagePage({Key? key, required this.equipage}) : super(key: key);

  Equipage equipage;

  @override
  State<ParameterEquipagePage> createState() => _ParameterEquipagePageState();
}

class _ParameterEquipagePageState extends State<ParameterEquipagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: const Text("Paramètre de l'équipage"),
        hasActionButton: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          for (CrewUser u in widget.equipage.members)
            Center(child: Text(u.surnom))
        ],
      ),
    );
  }
}

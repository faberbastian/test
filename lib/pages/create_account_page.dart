import 'package:flutter/material.dart';
import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/utils.dart';
import 'package:myapp_test/components/appbar_custom.dart';
import 'package:myapp_test/components/colorful_bubble.dart';
import 'package:myapp_test/components/footer_bottom_buttons.dart';
import 'package:myapp_test/components/textfield_custom.dart';
import 'package:myapp_test/pages/confirm_account_page.dart';

class CreateAccountPage extends StatefulWidget {
  const CreateAccountPage({Key? key, this.user}) : super(key: key);

  final User? user;

  @override
  State<StatefulWidget> createState() {
    return CreateAccountPageState();
  }
}

class CreateAccountPageState extends State<CreateAccountPage> {
  String initialesBulle = "?";

  String login = "";
  String password = "";
  bool samePassword = false;
  Color couleur = Colors.red.shade200;

  @override
  void initState() {
    super.initState();
    if (widget.user != null) {
      initialesBulle = getFirstsInitiales(widget.user!.surnom);
      couleur = widget.user!.couleur;
      login = widget.user!.surnom;
    }
  }

  _submit() {
    if (login.isNotEmpty && samePassword) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ConfirmAccountPage(
            user: User(surnom: login, couleur: couleur),
          ),
        ),
      );
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: const Text(
          "créer un nouveau compte",
          style: TextStyle(fontSize: 17.0),
        ),
        hasActionButton: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ColorfulBubble(
              title: initialesBulle,
              onChanged: (value) => {couleur = value},
              initialColor: couleur,
            ),
            TextFieldCustom(
              initialValue: login.isEmpty ? null : login,
              placeholder: "pseudo",
              maxLength: 20,
              onInput: (value, _) => setState(() {
                if (value.isEmpty) {
                  initialesBulle = "?";
                } else {
                  initialesBulle = getFirstsInitiales(value);
                }
                login = value;
              }),
              onSubmitted: (value, _) => _submit(),
            ),
            TextFieldCustom(
              placeholder: "Mot de passe",
              hideContent: true,
              margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
              onInput: (value, _) => {password = value},
              onSubmitted: (value, _) => _submit(),
            ),
            TextFieldCustom(
              placeholder: "Confirmer le mot de passe",
              hideContent: true,
              margin: const EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
              onInput: (value, _) => {samePassword = (value == password)},
              onSubmitted: (value, _) => _submit(),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
      bottomNavigationBar: FooterBottomButtons(
        disabled: const [true, false],
        onPressedNext: () => _submit(),
      ),
    );
  }
}

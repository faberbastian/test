import 'package:flutter/material.dart';
import 'package:myapp_test/components/bouton_icon.dart';

class ConnexionPage extends StatefulWidget {
  const ConnexionPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ConnexionPageState();
  }
}

class ConnexionPageState extends State<ConnexionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: Text(
              "A l'abordage !",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            )),
        BoutonIcon(
          key: const Key("bouton_to_equipages"),
          message: "Se connecter",
          imageURL: "assets/images/pirate_icon.png",
          onPressed: () {
            Navigator.pushNamed(context, '/equipages');
          },
        ),
        BoutonIcon(
          key: const Key("bouton_to_create_account"),
          message: "Créer un nouveau compte",
          imageURL: "assets/images/boue_icon.png",
          onPressed: () {
            Navigator.pushNamed(context, '/create_account');
          },
        ),
      ],
    )));
  }
}

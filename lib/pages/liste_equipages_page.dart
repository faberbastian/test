import 'package:flutter/material.dart';
import 'package:myapp_test/classes/bdd.dart';
import 'package:myapp_test/classes/equipage.dart';
import 'package:myapp_test/components/appbar_custom.dart';
import 'package:myapp_test/components/equipage_button.dart';
import 'package:myapp_test/components/small_button.dart';

class ListEquipagePage extends StatefulWidget {
  const ListEquipagePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ListEquipagePageState();
  }
}

class ListEquipagePageState extends State<ListEquipagePage> {
  List<Equipage> selfEquipages = const [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: const Text(
          "Vos équipages",
          style: TextStyle(fontSize: 17.0),
        ),
        hasLeading: false,
        actionButton: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/parameter');
            },
            icon: const Icon(Icons.settings)),
      ),
      body: Column(
        children: <Widget>[
          for (Equipage e in bddEquipages) EquipageButton(equipage: e),
          SmallButton(
            onPressed: () {},
            message: "créer un nouvel équipage",
          )
        ],
      ),
    );
  }
}

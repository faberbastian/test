import 'package:flutter/material.dart';
import 'package:myapp_test/classes/bdd.dart';
import 'package:myapp_test/classes/equipage.dart';
import 'package:myapp_test/classes/message.dart';
import 'package:myapp_test/components/appbar_custom.dart';
import 'package:myapp_test/components/div.dart';
import 'package:myapp_test/components/message_component.dart';
import 'package:myapp_test/components/textfield_custom.dart';
import 'package:myapp_test/pages/parameter_equipage_page.dart';

class EquipagePage extends StatefulWidget {
  EquipagePage({Key? key, required this.equipage}) : super(key: key);

  Equipage equipage;

  @override
  State<EquipagePage> createState() => _EquipagePageState();
}

class _EquipagePageState extends State<EquipagePage> {
  final _controller = ScrollController();

  void _scrollToTheBottom() {
    _controller.animateTo(_controller.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500), curve: Curves.easeInCubic);
  }

  void _submitMessage(value) {
    setState(() {
      if ((value as String).isNotEmpty) {
        widget.equipage.messages.add(Message(value, sender: currentUser));
      }
    });
  }

  void _onInput(value) {
    setState(() {
      _scrollToTheBottom();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarCustom(
          title: Text(widget.equipage.nom),
          actionButton: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ParameterEquipagePage(
                      equipage: widget.equipage,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.menu)),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              controller: _controller,
              child: Column(children: [
                const SizedBox(
                  height: 10.0,
                ),
                for (int i = 0; i < widget.equipage.messages.length; i++)
                  MessageComponent(
                    message: widget.equipage.messages[i],
                    selfSender:
                        widget.equipage.messages[i].sender == currentUser,
                    concatBottom: i < widget.equipage.messages.length - 1 &&
                        widget.equipage.messages[i].sender ==
                            widget.equipage.messages[i + 1].sender,
                    concatTop: i > 0 &&
                        widget.equipage.messages[i].sender ==
                            widget.equipage.messages[i - 1].sender,
                  ),
                const SizedBox(
                  height: 200.0,
                )
              ]),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: IntrinsicHeight(
                child: ColoredBox(
                  color: Colors.white,
                  child: TextFieldCustom(
                    placeholder: "votre message ici",
                    margin: const EdgeInsets.all(10.0),
                    onInput: (value, controller) => _onInput(value),
                    onSubmitted: (value, controller) => _submitMessage(value),
                    onTap: () {
                      _scrollToTheBottom();
                    },
                  ),
                ),
              ),
            )
          ],
        ));
  }
}

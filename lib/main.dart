import 'package:flutter/material.dart';
import 'package:myapp_test/pages/confirm_account_page.dart';
import 'package:myapp_test/pages/create_account_page.dart';
import 'package:myapp_test/pages/liste_equipages_page.dart';
import 'package:myapp_test/pages/connexion_page.dart';
import 'package:myapp_test/pages/parameter_page.dart';
import 'package:myapp_test/classes/bdd.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const ConnexionPage(),
        '/equipages': (context) => const ListEquipagePage(),
        '/connexion': (context) => const ConnexionPage(),
        '/parameter': (context) => const ParameterPage(),
        '/create_account': (context) => const CreateAccountPage(),
      },
      //const MyHomePage(title: 'Coucou les amis'),
    );
  }
}

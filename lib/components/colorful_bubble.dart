import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp_test/components/bulle.dart';
import 'package:myapp_test/components/colorful_button.dart';
import 'package:myapp_test/components/div.dart';

class ColorfulBubble extends StatefulWidget {
  ColorfulBubble(
      {Key? key, this.title = "?", this.onChanged, this.initialColor})
      : super(key: key);

  String title;
  final Function(Color)? onChanged;
  final Color? initialColor;

  @override
  State<StatefulWidget> createState() {
    return ColorfulBubbleState();
  }
}

class ColorfulBubbleState extends State<ColorfulBubble> {
  List<Color> colors = [
    Colors.red.shade200,
    Colors.orange.shade200,
    Colors.yellow.shade300,
    Colors.green.shade200,
    Colors.cyan.shade300,
    Colors.blue.shade300,
    Colors.purple.shade300,
    Colors.pink.shade200
  ];
  Color currentCouleur = Colors.red.shade200;

  @override
  void initState() {
    super.initState();

    if (widget.initialColor != null) {
      setState(() {
        currentCouleur = widget.initialColor!;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 20.0,
            ),
            child: Bulle(
              message: widget.title,
              color: Colors.white,
              fontSize: 45,
              bg: currentCouleur,
              circle: true,
              size: 150,
            ),
          ),
          Div(
            pressable: false,
            onClick: () {},
            borderRadius: 20.0,
            padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 5.0),
            child: Row(mainAxisSize: MainAxisSize.min, children: [
              for (Color c in colors)
                ColorfulButton(
                  couleur: c,
                  onChanged: (value) {
                    setState(() {
                      currentCouleur = value;
                      if (widget.onChanged != null) {
                        widget.onChanged!(currentCouleur);
                      }
                    });
                  },
                )
            ]),
          ),
        ],
      ),
    );
  }
}

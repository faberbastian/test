import 'package:flutter/material.dart';

class SmallButton extends OutlinedButton {
  const SmallButton(
      {Key? key,
      required onPressed,
      child = const Text("Un petit bouton"),
      this.message = "",
      this.mainColor = Colors.black54})
      : super(key: key, onPressed: onPressed, child: child);

  final String message;
  final Color mainColor;

  @override
  State<SmallButton> createState() {
    return SmallButtonState();
  }
}

class SmallButtonState extends State<SmallButton> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: OutlinedButton(
          onPressed: widget.onPressed,
          style: OutlinedButton.styleFrom(
              elevation: 0.0,
              shadowColor: widget.mainColor,
              side: BorderSide(color: widget.mainColor, width: 1)),
          child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: widget.message.isNotEmpty
                  ? Text(
                      widget.message,
                      style: TextStyle(color: widget.mainColor),
                    )
                  : widget.child)),
    ));
  }
}

import 'package:flutter/material.dart';

class AppBarCustom extends AppBar {
  AppBarCustom(
      {Key? key,
      title = const Text("Un titre"),
      this.hasActionButton = true,
      this.hasLeading = true,
      this.actionButton = const Icon(Icons.menu)})
      : super(key: key, title: title);

  final Widget actionButton;
  final bool hasActionButton;
  final bool hasLeading;

  @override
  State<AppBar> createState() {
    return AppBarCustomState();
  }
}

class AppBarCustomState extends State<AppBarCustom> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: widget.title,
      actions: widget.hasActionButton ? <Widget>[widget.actionButton] : [],
      backgroundColor: Colors.white,
      foregroundColor: Colors.black,
      leading: widget.hasLeading
          ? IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          : const Text(""),
      leadingWidth: widget.hasLeading ? null : 0,
      shadowColor: Colors.white,
    );
  }
}

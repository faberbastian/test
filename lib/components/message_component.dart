import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:myapp_test/classes/message.dart';
import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/utils.dart';
import 'package:myapp_test/components/bulle.dart';
import 'package:myapp_test/components/div.dart';

class MessageComponent extends StatelessWidget {
  MessageComponent(
      {Key? key,
      required this.message,
      this.selfSender = false,
      this.concatBottom = false,
      this.concatTop = false})
      : super(key: key) {
    message.sender ??= User(surnom: "Un utilisateur");
  }

  final Message message;
  final bool selfSender, concatBottom, concatTop;

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const SizedBox(
            width: 8,
          ),
          !selfSender
              ? Bulle(
                  message: getFirstsInitiales(message.sender!.surnom),
                  bg: message.sender!.couleur,
                  circle: true,
                  size: 40,
                )
              : const Text(""),
          const SizedBox(
            width: 8,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment:
                selfSender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              concatTop
                  ? const SizedBox()
                  : const SizedBox(
                      height: 15.0,
                    ),
              Div(
                borderRadius: 20.0,
                child: Text(
                  message.content,
                  overflow: TextOverflow.clip,
                  textAlign: selfSender ? TextAlign.right : TextAlign.left,
                  style: TextStyle(
                      color: selfSender ? Colors.white : Colors.black),
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 15.0),
                margin: const EdgeInsets.only(right: 10.0),
                bg: selfSender
                    ? const Color.fromRGBO(94, 120, 255, 1)
                    : Colors.white,
              ),
              const SizedBox(
                height: 3,
              ),
              concatBottom
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 20.0),
                      child: Text(
                        message.date.toString(),
                        style: const TextStyle(fontSize: 9),
                      ))
            ],
          )),
        ],
      ),
    );
  }
}

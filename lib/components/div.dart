import 'package:flutter/material.dart';

class Div extends Container {
  Div(
      {Key? key,
      this.borderRadius = 10.0,
      this.boxShadowActive = true,
      this.bg = Colors.white,
      Widget? child,
      EdgeInsets? margin,
      EdgeInsets? padding,
      this.pressable = false,
      this.onClick})
      : super(
          key: key,
          child: child,
          margin: margin,
          padding: padding,
        );

  final double borderRadius;
  final bool boxShadowActive;
  final Color bg;
  final bool pressable;
  Function()? onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
        onTap: onClick != null
            ? () {
                onClick!();
              }
            : null,
        child: Container(
            child: child,
            margin: margin,
            padding: padding,
            decoration: BoxDecoration(
                color: bg,
                borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
                boxShadow: boxShadowActive
                    ? [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 0,
                            blurRadius: 4,
                            offset: const Offset(0, 0))
                      ]
                    : [])));
  }
}

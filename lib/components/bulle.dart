import 'package:flutter/material.dart';
import 'package:myapp_test/classes/bdd.dart';

class Bulle extends StatefulWidget {
  const Bulle(
      {Key? key,
      required this.message,
      this.circle = false,
      this.borderRadius = 12.0,
      this.color = Colors.white,
      this.bg = Colors.red,
      this.fontSize = 20.0,
      this.size = 50.0,
      this.translationX = 0.0,
      this.subtext})
      : super(key: key);

  final String message;
  final bool circle;
  final double borderRadius;
  final Color bg;
  final Color color;
  final double fontSize;
  final double size;
  final double translationX;
  final String? subtext;

  @override
  State<Bulle> createState() {
    return BulleState();
  }
}

class BulleState extends State<Bulle> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Text(widget.message,
              style: TextStyle(
                  color: widget.color,
                  fontSize: widget.fontSize,
                  fontWeight: FontWeight.bold,
                  fontFamily: "inter")),
          decoration: BoxDecoration(
              color: widget.bg,
              borderRadius: BorderRadius.all(Radius.circular(
                  widget.circle ? widget.size : widget.borderRadius))),
          width: widget.size,
          height: widget.size,
          alignment: Alignment.center,
          margin: const EdgeInsets.only(
              top: 5.0, left: 5.0, bottom: 5.0, right: 5.0),
          transform: Matrix4.translationValues(widget.translationX, 0.0, 0.0),
        ),
        if (widget.subtext != null)
          Text(
            widget.subtext!,
            style: const TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
          ),
      ],
    );
  }
}

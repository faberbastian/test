import 'package:flutter/material.dart';

class ColorfulButton extends StatefulWidget {
  ColorfulButton({Key? key, this.couleur = Colors.red, this.onChanged})
      : super(key: key);

  final Color couleur;
  final Function(Color)? onChanged;
  bool active = false;

  change(bool activated) {}

  @override
  State<StatefulWidget> createState() {
    return ColorfulButtonState();
  }
}

class ColorfulButtonState extends State<ColorfulButton> {
  //bool active = false;

  _changed() {
    if (widget.onChanged != null) {
      widget.onChanged!(widget.couleur);
    }
    setState(() {
      widget.active = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double size = 20.0;
    return InkWell(
      onTap: _changed,
      borderRadius: BorderRadius.all(Radius.circular(size)),
      child: Container(
        width: size,
        height: size,
        margin: const EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            border:
                Border.all(color: Colors.white, width: widget.active ? 5.0 : 0),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 0,
                  blurRadius: 4,
                  offset: const Offset(0, 0))
            ],
            color: widget.couleur,
            borderRadius: BorderRadius.all(Radius.circular(size))),
      ),
    );
  }
}

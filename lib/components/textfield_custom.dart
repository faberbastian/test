import 'package:flutter/material.dart';

class TextFieldCustom extends StatefulWidget {
  TextFieldCustom(
      {Key? key,
      this.maxLength = -1,
      this.minLength = 0,
      this.margin = const EdgeInsets.only(left: 10.0, right: 10.0, top: 30.0),
      this.fontSize = 15.0,
      this.bg = const Color.fromRGBO(240, 240, 240, 1),
      this.borderRadius = 10.0,
      required this.placeholder,
      this.contentPadding = const EdgeInsets.symmetric(horizontal: 15.0),
      this.hideContent = false,
      this.onInput,
      this.onSubmitted,
      this.onTap,
      this.errorText = "",
      this.initialValue})
      : super(key: key);

  final int maxLength, minLength;
  final EdgeInsets margin, contentPadding;
  final double fontSize, borderRadius;
  final Color bg;
  final String placeholder;
  final bool hideContent;
  Function(String, dynamic)? onInput;
  Function(String, dynamic)? onSubmitted;
  Function()? onTap;
  final String? initialValue;
  String errorText;

  @override
  State<StatefulWidget> createState() {
    return TextFieldCustomState();
  }
}

class TextFieldCustomState extends State<TextFieldCustom> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      _controller.text = widget.initialValue!;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String? get _errorText {
    String text = _controller.value.text;
    if (text.length < widget.minLength && text.isNotEmpty) {
      return '"' + widget.placeholder + '" est trop court';
    } else if (widget.maxLength != -1 && text.length > widget.maxLength) {
      return '"' + widget.placeholder + '" est trop long';
    } else {
      return null;
    }
  }

  @override
  Widget build(context) {
    return Padding(
        padding: widget.margin,
        child: TextField(
          controller: _controller,
          maxLength: widget.maxLength == -1 ? null : widget.maxLength,
          onChanged: (value, [_controller]) {
            widget.onInput!(value, [_controller]);
            setState(() {});
          },
          onSubmitted: (value, [_controller]) {
            widget.onSubmitted!(value, [_controller]);
          },
          onTap: () {
            widget.onTap!();
          },
          obscureText: widget.hideContent,
          style: TextStyle(fontSize: widget.fontSize),
          decoration: InputDecoration(
              errorText: _errorText,
              filled: true,
              fillColor: widget.bg,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(widget.borderRadius),
                  borderSide:
                      const BorderSide(color: Colors.transparent, width: 0.0)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(widget.borderRadius),
                  borderSide:
                      const BorderSide(color: Colors.transparent, width: 0.0)),
              labelText: widget.placeholder,
              contentPadding: widget.contentPadding),
        ));
  }
}

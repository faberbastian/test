import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  BottomButton(
      {Key? key,
      this.message = "Suivant",
      this.onPressed,
      this.mainCouleur = true,
      this.disabled = false})
      : super(key: key);

  final String message;
  Function()? onPressed;
  final bool mainCouleur;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          if (onPressed != null && !disabled) onPressed!();
        },
        child: Text(
          message,
          style:
              const TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
      ),
      margin: const EdgeInsets.all(20.0),
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      decoration: BoxDecoration(
        color: disabled
            ? const Color.fromRGBO(231, 231, 231, 1)
            : (mainCouleur
                ? const Color.fromRGBO(46, 117, 255, 1)
                : const Color.fromRGBO(193, 193, 193, 1)),
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 0,
              blurRadius: 4,
              offset: const Offset(0, 0))
        ],
      ),
    );
  }
}

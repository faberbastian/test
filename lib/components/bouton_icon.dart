import 'package:flutter/material.dart';
import 'package:myapp_test/components/div.dart';

class BoutonIcon extends Container {
  BoutonIcon(
      {Key? key,
      this.message = "Bouton avec icone",
      this.icon = Icons.no_encryption,
      this.imageURL = "",
      required this.onPressed})
      : super(key: key);

  final String message;
  final String imageURL;
  final IconData icon;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Div(
      child: Row(
        children: [
          Container(
            child: imageURL.isEmpty
                ? Icon(icon)
                : Image.asset(
                    imageURL,
                    width: 25,
                  ),
            padding: const EdgeInsets.all(15.0),
            margin: const EdgeInsets.only(right: 10.0),
            decoration: const BoxDecoration(
                border: Border(
                    right: BorderSide(
                        color: Colors.black26, style: BorderStyle.solid))),
          ),
          Flexible(
            child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  message,
                  overflow: TextOverflow.clip,
                )),
          )
        ],
      ),
      padding: const EdgeInsets.all(0.0),
      margin: const EdgeInsets.all(10.0),
      pressable: true,
      onClick: () {
        onPressed();
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:myapp_test/components/bottom_button.dart';

class FooterBottomButtons extends StatelessWidget {
  FooterBottomButtons(
      {Key? key,
      this.onPressedPrec,
      this.onPressedNext,
      this.disabled = const [false, false],
      this.messages = const ["Précédent", "Suivant"]})
      : super(key: key);

  Function()? onPressedPrec;
  Function()? onPressedNext;
  final List<bool> disabled;
  final List<String> messages;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        BottomButton(
          message: messages[0],
          mainCouleur: false,
          disabled: disabled[0],
          onPressed: onPressedPrec,
        ),
        BottomButton(
          message: messages[1],
          onPressed: onPressedNext,
          disabled: disabled[1],
        ),
      ],
    );
  }
}

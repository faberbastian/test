import 'package:flutter/material.dart';
import 'package:myapp_test/classes/equipage.dart';
import 'package:myapp_test/classes/utils.dart';
import 'package:myapp_test/components/bulle.dart';
import 'package:myapp_test/pages/equipage_page.dart';

class EquipageButton extends Container {
  EquipageButton(
      {Key? key, required this.equipage, this.colorBubble = Colors.red})
      : super(key: key);

  final Equipage equipage;
  final int maxInitiales = 2;
  final Color colorBubble;

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];
    var i = 0;
    children.add(IconButton(
        padding: const EdgeInsets.only(top: 2.0),
        constraints: const BoxConstraints(),
        onPressed: () {},
        icon: const Icon(Icons.qr_code_scanner)));
    for (var u in equipage.members) {
      children.add(Bulle(
          message: getFirstsInitiales(u.surnom),
          size: 20.0,
          fontSize: 10.0,
          translationX: -15.0 * i,
          bg: u.getColor()));
      i += 1;
    }
    return Container(
      child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EquipagePage(
                  equipage: equipage,
                ),
              ),
            );
          },
          child: Row(
            children: <Widget>[
              Bulle(
                message: getFirstsInitiales(equipage.nom),
                bg: equipage.couleur,
              ),
              Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(equipage.nom,
                              style: const TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w500))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: children,
                      )
                    ],
                  ))
            ],
            mainAxisAlignment: MainAxisAlignment.start,
          )),
      padding: const EdgeInsets.all(8.0),
      margin:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 15.0, bottom: 0.0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0,
                blurRadius: 4,
                offset: const Offset(0, 0))
          ]), // changes position of shadow)]),
    );
  }
}

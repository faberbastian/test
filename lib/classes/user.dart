import 'package:flutter/material.dart';

class User {
  User({this.surnom = "Hamza la brute", this.couleur = Colors.red});

  String surnom;
  Color couleur;
  final int maxInitiales = 2;

  String getInitiales() {
    var mots = surnom.split(" ");
    int i = 0;
    String res = "";
    while (i < maxInitiales && i < mots.length) {
      res += mots[i][0].toUpperCase();
      i += 1;
    }
    return res;
  }

  Color getColor() {
    return couleur;
  }
}

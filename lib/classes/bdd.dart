import 'package:flutter/material.dart';
import 'package:myapp_test/classes/equipage.dart';
import 'package:myapp_test/classes/message.dart';
import 'package:myapp_test/classes/user.dart';

var users = <User>[
  User(couleur: Colors.green),
  User(surnom: "Xavier Sesboue", couleur: Colors.pink.shade100),
  User(surnom: "Sarah le Mee", couleur: Colors.purple.shade200),
  User(surnom: "Julie Allais", couleur: Colors.yellow)
];

var currentUser =
    User(surnom: "Utilisateur default", couleur: Colors.green.shade300);

var bddEquipages = <Equipage>[
  Equipage(
      couleur: Colors.green.shade300,
      nom: "Une premiere conv",
      users: [users[0], users[1]]),
  Equipage(
      couleur: Colors.yellow.shade300,
      nom: "Une seconde conv",
      users: [users[1], users[2]]),
  Equipage(couleur: Colors.red.shade300, nom: "Une derniere conv", users: [
    users[2],
    users[0],
    users[0]
  ], messages: [
    Message("coucou les amis", sender: users[0]),
    Message(
        "je suis un trèqs long message ohqzdid hqozidh qiozdhoqi hzdioh qoizdho qihzdioh qiozdhqio hzdoiqh izdoh qiozdhqozhdo ihqodihq iozdh iqzhdio qhzdioh qozihd oqihziod hqizdh iqohzdo qhzido hqoizdh oqhi",
        sender: users[1]),
    Message("je suis le user connecte", sender: currentUser),
    Message(
        "je suis aussi une très long message oqzihdpqozhd opqihzdpo qhizpdohiq zpodih qpozidh opqzhdop iqhdpo zqhdoi hqzopidh qpozidh pqoizdh qopizdh qopzihdpqozihdqopihzdpoqizhdqopihzd qopizdh ",
        sender: currentUser)
  ]),
];

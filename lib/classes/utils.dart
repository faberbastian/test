getFirstsInitiales(String message, {int maxInitiales = 2}) {
  if (message.contains(" ")) {
    List<String> mots = message.split(" ");
    int i = 0;
    String res = "";
    while (i < maxInitiales && i < mots.length) {
      if (mots[i].isNotEmpty) {
        res += mots[i][0].toUpperCase();
      }
      i += 1;
    }
    return res;
  } else {
    return message.isNotEmpty ? message[0].toUpperCase() : "";
  }
}

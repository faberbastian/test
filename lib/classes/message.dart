import 'package:myapp_test/classes/user.dart';

class Message {
  Message(this.content, {this.date, this.sender}) {
    date ??= DateTime.now();
  }

  String content;
  DateTime? date;
  User? sender;
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp_test/classes/message.dart';
import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/crew_user.dart';
import 'package:myapp_test/classes/roles.dart';

class Equipage {
  Equipage(
      {this.nom = "Une conversation",
      this.couleur = Colors.red,
      required List<User> users,
      this.messages = const []}) {
    for (var u in users) {
      ajouterMembre(u);
    }
  }

  String nom;
  List<CrewUser> members = [];
  Color couleur;
  List<Message> messages = [];

  void ajouterMembre(User user) {
    CrewUser crewUser = CrewUser(role: Roles.member, user: user);
    members.add(crewUser);
  }
}

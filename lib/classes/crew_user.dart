import 'package:myapp_test/classes/user.dart';
import 'package:myapp_test/classes/roles.dart';

class CrewUser extends User {
  CrewUser({this.role = Roles.member, required User user})
      : super(surnom: user.surnom, couleur: user.couleur);

  Roles role;

  changeRole(Roles role) {
    this.role = role;
  }
}

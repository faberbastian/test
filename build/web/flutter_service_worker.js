'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "test/.git/ORIG_HEAD": "3fd8fde19ed415394e68781fe1752d5f",
"test/.git/objects/dc/f6642c24d4c503434581308672bed01dfb0d67": "ac7c138d0973bd7e654c0b9d74760c79",
"test/.git/objects/dc/0008c740cbea5e66f79a4537852fea4cecbd50": "486eca779dbd27d04999337ab2c582ba",
"test/.git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
"test/.git/objects/03/eaddffb9c0e55fb7b5f9b378d9134d8d75dd37": "87850ce0a3dd72f458581004b58ac0d6",
"test/.git/objects/84/3f5de997c45ded1b6d2702e2716f35d509c573": "6d0901ae16a3844ce07ef476c6f71f67",
"test/.git/objects/2c/2f379161ab52be9c84504f583c198036c77dcb": "132e713053ee0bfa389f9b8889fc7455",
"test/.git/objects/37/bdbdbe9f01ff5b66412680ef0864b10c7798d6": "0fd35df8a2d1cc564349362509323ac5",
"test/.git/objects/74/f8df4fa5dc9a20e08b2a4de79acface91aa818": "8d0536fcaf1aee64a59efa51e521ab6d",
"test/.git/objects/ab/0e98497a51ead7821d1da35a24968ff314e50f": "557c35fe3928eb2af403d1b3926bb9ba",
"test/.git/objects/3f/dfb7cad2753fc50da4b249e36306c625552263": "0aeed591d33d190879ff10faf269f9f7",
"test/.git/objects/d9/e23cfe6e48dddb57f94de3c892f1bb3835801c": "a9b817eaf0a447cbc2ca904ad8a880c9",
"test/.git/objects/a1/3837a12450aceaa5c8e807c32e781831d67a8f": "bfe4910ea01eb3d69e9520c3b42a0adf",
"test/.git/objects/f9/d69a1afa728c83afc53d406a6c787acb5e0a23": "283a6ef5b32eebe06fd22464abe75385",
"test/.git/objects/29/02c29a49e5cfd91a46eb0716b233e566ff43a4": "73e7f5a5fe13f9319ea4e97e61ef16c4",
"test/.git/objects/93/10b3317fc6fc092269829eb6412553059ce802": "45f4882b8b930218d000691f580e6ca4",
"test/.git/objects/38/fb01bd77ac2acf02c7390012ccf40a100de257": "337b2335d837af1f760d1ef1fef67999",
"test/.git/objects/90/fa7a6fa9dbbd1a3a14696aeffaa02a32a21668": "f37a15dabf88a9234575f64d119d4abb",
"test/.git/objects/e5/900f8cf143cb03dd9a44d29d5e56a169da65c3": "34e7a1720b984544e480a5f76b1ffe7d",
"test/.git/objects/e5/951dfb943474a56e611d9923405cd06c2dd28d": "c6fa51103d8db5478e1a43a661f6c68d",
"test/.git/objects/30/f89e64c2d387564547564f8f9dce5aeeac3545": "5760539bb797ceb4ec428d373cc04bd0",
"test/.git/objects/11/0cf5abddeaeb4e55ed71f36a1d7ca93683d19e": "3b95828f6d2da0ee54109e1c27f4952c",
"test/.git/objects/a8/beffd3ad4fe54d6cabccf83a05477d6a986cd0": "6677888e4a051c7838b5b240c09f0981",
"test/.git/objects/79/ba7ea0836b93b3f178067bcd0a0945dbc26b3f": "f3e31aec622d6cf63f619aa3a6023103",
"test/.git/objects/46/4ab5882a2234c39b1a4dbad5feba0954478155": "2e52a767dc04391de7b4d0beb32e7fc4",
"test/.git/objects/c3/92ec921f03a0b9b549b8ac3ab17bf645ba2931": "2f386e18eb4e6a48e36d44e3f52c3c21",
"test/.git/objects/26/fa5c4b9063ba434ef3aad19e78919273d3bcb5": "b710d13e882190f9722b5bd9ef3591d0",
"test/.git/objects/ad/8174d3e3649c8b191fe1b0ea0e3dd336922997": "f09993e837f76db1ad906bf58a7bad2d",
"test/.git/objects/ad/a871bdc824c8fd171fb1c74a61f7304d189ff3": "2397dd374a8e3d928fd43ffb1ae7c012",
"test/.git/objects/d3/dab8b160435acc6e70a59812d8391a24fa1ddc": "0575056f4ec6d8e85335cb475ecc71f6",
"test/.git/objects/8d/b3fbd62f894008d8cbef82e0c058019c210f72": "7ea7e6c4ace970110aa1dbc5666c2867",
"test/.git/objects/92/f1f36e7fcef66131715b25ab8ce75644a05d37": "240ddc7fc0d8e2d5f622b6cd50145aad",
"test/.git/objects/9b/d5739cb6ecf1aa74706a0f71c7c4270b3a70e8": "f4c5bcc13447ec7b56d862de5d597316",
"test/.git/objects/a6/3c67b1466ee5c6b3fd589a16dc9242d07a130c": "d9f30db4803f71e5ae369628ecbc4d08",
"test/.git/objects/b9/2a0d854da9a8f73216c4a0ef07a0f0a44e4373": "f62d1eb7f51165e2a6d2ef1921f976f3",
"test/.git/objects/36/b9fbb51027f98503944f4f9e5ad9050b3550d3": "947a33a1598bef5595b208cdeef95646",
"test/.git/objects/9a/2e0dc2dbbf565667fe4fd63478683e43d83243": "e0d077497ecaa4ec16ae1b631599ce2f",
"test/.git/objects/66/a934961748d9dc828c3412a90a6e2b2f69ee67": "5f99970b9c71008499705984370ba1e4",
"test/.git/objects/20/554d3c04f3b52757a4ae27253aebb1e57b0eeb": "f07e4bbdbc95beff35cb01488256e46f",
"test/.git/objects/c2/ccb49ca6052b31611c0b6e33b9ab625bb14ab6": "866c833ef81408cf35b87812d2f72dfd",
"test/.git/objects/7c/4c0b935f0b8267bd588fc566a694db37f56e1d": "e015b528f076f8952f57e570ed959e14",
"test/.git/objects/89/dae4ff870a6d7d4bb0ccf5fc5d1f7e56d0614e": "f185acf8a51e643c402b81e1b7cf5aae",
"test/.git/objects/d6/dd048cc23f68fd87df06cda6ec77f94b105686": "0957559bf7ab85f885dfe534ae1d6ea7",
"test/.git/objects/d6/9c56691fbdb0b7efa65097c7cc1edac12a6d3e": "868ce37a3a78b0606713733248a2f579",
"test/.git/objects/d6/70c6bc3157fe5d0b370e852ca91224c0fe2fa7": "3977861fee792cd8a175824ee0d7382b",
"test/.git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
"test/.git/objects/e9/0f9c6db12e2f89ff5a7b2053f1a08039f81283": "55c344fb69c25a6451cc6fe6017c754c",
"test/.git/objects/f2/7c4bd23885210bffe11138861632e35e81fb16": "4fa084b70b016aad89324f43a3130d28",
"test/.git/objects/f2/fbc0863cb1a0697bd30655ddaf5408159d395c": "98a96c9bdbb2af326b5ac9bf56b943ba",
"test/.git/objects/f6/2642f0d50f0a3ca2b0268103681df27772d248": "cfe21c310e438af0b4a9c4305de680c6",
"test/.git/objects/1b/eaaad034ff9cea05659d00732ce20d329416a1": "6c02fa3656be98f0ed00882f51844c72",
"test/.git/objects/40/2a4170d96cb544d532ec84ec783e8cede2fb52": "f2031c5a542e498ec35334b6f5211e5b",
"test/.git/objects/32/0c40c3a4bc511f1a6141d7b6eef353f453cc7a": "9b471aabe59b6df05f485730fd28e269",
"test/.git/objects/32/6492f866a282480d7ad3658078a3589f6641a9": "1dd18200db671a1e704449bac5e7a523",
"test/.git/objects/32/46ad559eeae0370195978eaed83f1053ee13fd": "a043dbc0a0bda96ce2127799ccc27506",
"test/.git/objects/eb/9b4d76e525556d5d89141648c724331630325d": "37c0954235cbe27c4d93e74fe9a578ef",
"test/.git/objects/45/4adcd11ef029f5afafb78334c65fe3c6a14ad8": "f6bc5271f84b1c5f1a24fa220514d5f7",
"test/.git/objects/45/e530cb657b03a97a7287655ee5189ea0668d85": "9dc0f141aa7d1eb27e75fbd7d6ef714c",
"test/.git/objects/8b/ab49e8db380cb934b2770200fc8c681c7b2926": "3ccb534bdcb2fc0dbda2c30a3f20c037",
"test/.git/objects/3e/aeeea7cc32c05defc94edbfa1f51068c192a87": "48786d9c8af5fc3060255f8e42a38f6b",
"test/.git/objects/3e/3d522662eee9e541692e4ef74eabfcc256d8f7": "06bd3127cdabff86028a70fccebadb99",
"test/.git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
"test/.git/COMMIT_EDITMSG": "58aedfbdaf775db6e5caa933083c70df",
"test/.git/logs/HEAD": "09e8cdaa9ab5cfff3638d0ed4eda4cc7",
"test/.git/logs/refs/heads/main": "09e8cdaa9ab5cfff3638d0ed4eda4cc7",
"test/.git/logs/refs/remotes/origin/main": "feb1c7c70ed756f5b78a3a71bda39cd0",
"test/.git/logs/refs/remotes/origin/HEAD": "9621e9321aac8450c8eb130251828374",
"test/.git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
"test/.git/index": "7263123ffc1f1d0d0d803e9b6a9f3214",
"test/.git/FETCH_HEAD": "a5a2c80a58a28518afce897c26dddd92",
"test/.git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
"test/.git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
"test/.git/hooks/pre-commit.sample": "01b1688f97f94776baae85d77b06048b",
"test/.git/hooks/pre-push.sample": "3c5989301dd4b949dfa1f43738a22819",
"test/.git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
"test/.git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
"test/.git/hooks/fsmonitor-watchman.sample": "ecbb0cb5ffb7d773cd5b2407b210cc3b",
"test/.git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
"test/.git/hooks/update.sample": "517f14b9239689dff8bda3022ebd9004",
"test/.git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
"test/.git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
"test/.git/HEAD": "cf7dd3ce51958c5f13fece957cc417fb",
"test/.git/refs/heads/main": "56391164fb254fc3ebce5e07a269e131",
"test/.git/refs/remotes/origin/main": "56391164fb254fc3ebce5e07a269e131",
"test/.git/refs/remotes/origin/HEAD": "98b16e0b650190870f1b40bc8f4aec4e",
"test/.git/config": "d8122c7e5e6b9b81dad80cfdc87e7865",
"test/.git/packed-refs": "6216dcf47462e0af7f47caf444b8bb30",
"test/.git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
"test/main.dart.js": "08ee4ecd5c215a931e860a9d2ef0ffb3",
"test/assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"test/assets/NOTICES": "172661f7a237bd44c35101faf594c151",
"test/assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"test/assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"test/assets/AssetManifest.json": "2efbb41d7877d10aac9d091f58ccd7b9",
"test/web/main.dart.js": "08ee4ecd5c215a931e860a9d2ef0ffb3",
"test/web/assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"test/web/assets/NOTICES": "172661f7a237bd44c35101faf594c151",
"test/web/assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"test/web/assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"test/web/assets/AssetManifest.json": "2efbb41d7877d10aac9d091f58ccd7b9",
"test/web/version.json": "a814bdcd7fdfda5b83c9b17e3ed0cdbe",
"test/web/icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"test/web/icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"test/web/icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"test/web/icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"test/web/canvaskit/profiling/canvaskit.js": "f3bfccc993a1e0bfdd3440af60d99df4",
"test/web/canvaskit/profiling/canvaskit.wasm": "a9610cf39260f60fbe7524a785c66101",
"test/web/canvaskit/canvaskit.js": "43fa9e17039a625450b6aba93baf521e",
"test/web/canvaskit/canvaskit.wasm": "04ed3c745ff1dee16504be01f9623498",
"test/web/index.html": "b23e91a59681828fb9c45d7d6658da06",
"/": "aa6df4019f5307f96f59056d5ae8e030",
"test/web/favicon.png": "5dcef449791fa27946b3d35ad8803796",
"test/web/manifest.json": "57995c0dca1b92e4bdec7931838e63ed",
"test/version.json": "a814bdcd7fdfda5b83c9b17e3ed0cdbe",
"test/icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"test/icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"test/icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"test/icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"test/canvaskit/profiling/canvaskit.js": "f3bfccc993a1e0bfdd3440af60d99df4",
"test/canvaskit/profiling/canvaskit.wasm": "a9610cf39260f60fbe7524a785c66101",
"test/canvaskit/canvaskit.js": "43fa9e17039a625450b6aba93baf521e",
"test/canvaskit/canvaskit.wasm": "04ed3c745ff1dee16504be01f9623498",
"test/index.html": "8cb1dd03aa34ad07a943922ff89ae138",
"test/favicon.png": "5dcef449791fa27946b3d35ad8803796",
"test/manifest.json": "57995c0dca1b92e4bdec7931838e63ed",
"test/README.md": "5f2d0bd6567770f0d77c8d12b888c877",
"main.dart.js": "2a2b0c497a4e8f20d2daec3a97afaef5",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/NOTICES": "172661f7a237bd44c35101faf594c151",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/assets/images/boue_icon.png": "3fc7de42b8afcfaccc6477a189ebbe77",
"assets/assets/images/pirate_icon.png": "6f8211666e6eb086162bd6d42a8f5416",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/AssetManifest.json": "92501b82b2636220c2e2d2a2ab5f051c",
"version.json": "a814bdcd7fdfda5b83c9b17e3ed0cdbe",
"demoapp/.git/ORIG_HEAD": "3338b34458162822f7b08254191ef22e",
"demoapp/.git/objects/dc/823976198529a0daf076867c4b7862eef0b51e": "e048d36001144af83e44b45f75d0dbe1",
"demoapp/.git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
"demoapp/.git/objects/03/eaddffb9c0e55fb7b5f9b378d9134d8d75dd37": "87850ce0a3dd72f458581004b58ac0d6",
"demoapp/.git/objects/03/9c82e8e4be99751463a34cfe5816737eed1076": "9f7c6db57574da5e8601237bbffc8755",
"demoapp/.git/objects/ab/0e98497a51ead7821d1da35a24968ff314e50f": "557c35fe3928eb2af403d1b3926bb9ba",
"demoapp/.git/objects/a1/3837a12450aceaa5c8e807c32e781831d67a8f": "bfe4910ea01eb3d69e9520c3b42a0adf",
"demoapp/.git/objects/06/c84909c37b291069b948d29f693ed4659afd90": "e6788ae0819f83b9963bb195730cb90d",
"demoapp/.git/objects/90/fa7a6fa9dbbd1a3a14696aeffaa02a32a21668": "f37a15dabf88a9234575f64d119d4abb",
"demoapp/.git/objects/e5/900f8cf143cb03dd9a44d29d5e56a169da65c3": "34e7a1720b984544e480a5f76b1ffe7d",
"demoapp/.git/objects/e5/951dfb943474a56e611d9923405cd06c2dd28d": "c6fa51103d8db5478e1a43a661f6c68d",
"demoapp/.git/objects/27/437b9c7efb10ba99c0432c16aa2c131bc4449a": "04cb1023bfdeeae6bfbb5f129e786e98",
"demoapp/.git/objects/d2/7953b739df4e4ef1bae745459c63b85a52ff17": "55ed6b19fccc7260996d442ee7b89cdc",
"demoapp/.git/objects/d1/76ce1929911c4ba5d865347f1d56ea04080f2d": "dd1a09a0ed5a610cb76bb79d302bf4e4",
"demoapp/.git/objects/21/fb037c7cfe397ebe57df857b9b6686f89c69aa": "fbeed4b21d0a1d0a8bdec99df302539e",
"demoapp/.git/objects/a8/beffd3ad4fe54d6cabccf83a05477d6a986cd0": "6677888e4a051c7838b5b240c09f0981",
"demoapp/.git/objects/79/ba7ea0836b93b3f178067bcd0a0945dbc26b3f": "f3e31aec622d6cf63f619aa3a6023103",
"demoapp/.git/objects/e8/555c7c879638d704a3193a10c65334dcd11a81": "60f357e1044f0d70bf82b51881a597d3",
"demoapp/.git/objects/46/4ab5882a2234c39b1a4dbad5feba0954478155": "2e52a767dc04391de7b4d0beb32e7fc4",
"demoapp/.git/objects/56/3c40c5d4c9d1246d42b0ff272ee40170574243": "eb8cb70c117dbadd266a7df014b15c98",
"demoapp/.git/objects/0b/919a048e80c6714249977e0e14db61bf8b4053": "55fd264861808ad5a2643d894ae0a491",
"demoapp/.git/objects/2d/ab3fb5d3ed5c95df83656ae458af5e129c6ff2": "8b13872d6f8cad9cacd877444faf4c57",
"demoapp/.git/objects/26/fa5c4b9063ba434ef3aad19e78919273d3bcb5": "b710d13e882190f9722b5bd9ef3591d0",
"demoapp/.git/objects/ad/8174d3e3649c8b191fe1b0ea0e3dd336922997": "f09993e837f76db1ad906bf58a7bad2d",
"demoapp/.git/objects/ad/a871bdc824c8fd171fb1c74a61f7304d189ff3": "2397dd374a8e3d928fd43ffb1ae7c012",
"demoapp/.git/objects/8d/b3fbd62f894008d8cbef82e0c058019c210f72": "7ea7e6c4ace970110aa1dbc5666c2867",
"demoapp/.git/objects/b0/ccd6c75fd25a51481e05625723a0100267667c": "f82d3099da92e54390126140c7d1641a",
"demoapp/.git/objects/22/b572c15729d5c32c777dbeb1da54a28fcae9a3": "14394c13c329e5c68ad1d48552a90e23",
"demoapp/.git/objects/9b/d5739cb6ecf1aa74706a0f71c7c4270b3a70e8": "f4c5bcc13447ec7b56d862de5d597316",
"demoapp/.git/objects/c8/4fb7bf80b398028d20222ea2cacde57d1fb546": "41bc560d46a03241bd2a392e287cf97d",
"demoapp/.git/objects/b9/2a0d854da9a8f73216c4a0ef07a0f0a44e4373": "f62d1eb7f51165e2a6d2ef1921f976f3",
"demoapp/.git/objects/36/b9fbb51027f98503944f4f9e5ad9050b3550d3": "947a33a1598bef5595b208cdeef95646",
"demoapp/.git/objects/c2/ccb49ca6052b31611c0b6e33b9ab625bb14ab6": "866c833ef81408cf35b87812d2f72dfd",
"demoapp/.git/objects/7c/4c0b935f0b8267bd588fc566a694db37f56e1d": "e015b528f076f8952f57e570ed959e14",
"demoapp/.git/objects/89/d533e50ec3edaf1b3916b80bebdb35ddf9eae8": "20f278221aae88fea0bf152e744aeb10",
"demoapp/.git/objects/89/dae4ff870a6d7d4bb0ccf5fc5d1f7e56d0614e": "f185acf8a51e643c402b81e1b7cf5aae",
"demoapp/.git/objects/5f/940e577965de70b39653a8e9e24f81b5a1093a": "5e00af463a3eba4bf13d3d3f358c5adb",
"demoapp/.git/objects/d6/9c56691fbdb0b7efa65097c7cc1edac12a6d3e": "868ce37a3a78b0606713733248a2f579",
"demoapp/.git/objects/d6/70c6bc3157fe5d0b370e852ca91224c0fe2fa7": "3977861fee792cd8a175824ee0d7382b",
"demoapp/.git/objects/88/797c443de21aa4fe9c0aae85814434feb0f5e7": "4489f48956219297f3d23271d7e03f1d",
"demoapp/.git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
"demoapp/.git/objects/f2/7c4bd23885210bffe11138861632e35e81fb16": "4fa084b70b016aad89324f43a3130d28",
"demoapp/.git/objects/f6/2642f0d50f0a3ca2b0268103681df27772d248": "cfe21c310e438af0b4a9c4305de680c6",
"demoapp/.git/objects/62/a180530e40363b454fc39e89b2ed2864feb6c2": "c5120ce2fc0e16372bd72a0d9ab9f842",
"demoapp/.git/objects/32/46ad559eeae0370195978eaed83f1053ee13fd": "a043dbc0a0bda96ce2127799ccc27506",
"demoapp/.git/objects/eb/9b4d76e525556d5d89141648c724331630325d": "37c0954235cbe27c4d93e74fe9a578ef",
"demoapp/.git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
"demoapp/.git/COMMIT_EDITMSG": "64d42024f1a77ee5e61e4096bdebac78",
"demoapp/.git/logs/HEAD": "fd9ced69b5d5d2da0e51a3b113a6b1e5",
"demoapp/.git/logs/refs/heads/main": "fd9ced69b5d5d2da0e51a3b113a6b1e5",
"demoapp/.git/logs/refs/remotes/origin/main": "513ce3cabcf79cd75f4a7d96266eddba",
"demoapp/.git/logs/refs/remotes/origin/HEAD": "4e38f52295b6c56dcaf879bac05a1a49",
"demoapp/.git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
"demoapp/.git/index": "873a1ef0ba120144f6871fd3498e1521",
"demoapp/.git/FETCH_HEAD": "4087e6741e107364b2b6c7facf8e44c1",
"demoapp/.git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
"demoapp/.git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
"demoapp/.git/hooks/pre-commit.sample": "01b1688f97f94776baae85d77b06048b",
"demoapp/.git/hooks/pre-push.sample": "3c5989301dd4b949dfa1f43738a22819",
"demoapp/.git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
"demoapp/.git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
"demoapp/.git/hooks/fsmonitor-watchman.sample": "ecbb0cb5ffb7d773cd5b2407b210cc3b",
"demoapp/.git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
"demoapp/.git/hooks/update.sample": "517f14b9239689dff8bda3022ebd9004",
"demoapp/.git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
"demoapp/.git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
"demoapp/.git/HEAD": "cf7dd3ce51958c5f13fece957cc417fb",
"demoapp/.git/refs/heads/main": "5dd13f816b8c7ca8de0067567a6146fa",
"demoapp/.git/refs/remotes/origin/main": "5dd13f816b8c7ca8de0067567a6146fa",
"demoapp/.git/refs/remotes/origin/HEAD": "98b16e0b650190870f1b40bc8f4aec4e",
"demoapp/.git/config": "6e3796416ebbd395ded2613f9a8d6e84",
"demoapp/.git/packed-refs": "a0354a20b42ab24b902bbb53c55ee9b8",
"demoapp/.git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
"demoapp/web/main.dart.js": "08ee4ecd5c215a931e860a9d2ef0ffb3",
"demoapp/web/assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"demoapp/web/assets/NOTICES": "172661f7a237bd44c35101faf594c151",
"demoapp/web/assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"demoapp/web/assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"demoapp/web/assets/AssetManifest.json": "2efbb41d7877d10aac9d091f58ccd7b9",
"demoapp/web/version.json": "a814bdcd7fdfda5b83c9b17e3ed0cdbe",
"demoapp/web/icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"demoapp/web/icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"demoapp/web/icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"demoapp/web/icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"demoapp/web/canvaskit/profiling/canvaskit.js": "f3bfccc993a1e0bfdd3440af60d99df4",
"demoapp/web/canvaskit/profiling/canvaskit.wasm": "a9610cf39260f60fbe7524a785c66101",
"demoapp/web/canvaskit/canvaskit.js": "43fa9e17039a625450b6aba93baf521e",
"demoapp/web/canvaskit/canvaskit.wasm": "04ed3c745ff1dee16504be01f9623498",
"demoapp/web/index.html": "b23e91a59681828fb9c45d7d6658da06",
"demoapp/web/favicon.png": "5dcef449791fa27946b3d35ad8803796",
"demoapp/web/manifest.json": "57995c0dca1b92e4bdec7931838e63ed",
"demoapp/README.md": "22975b067336b3a02d8725f9e4dc446f",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"canvaskit/profiling/canvaskit.js": "f3bfccc993a1e0bfdd3440af60d99df4",
"canvaskit/profiling/canvaskit.wasm": "a9610cf39260f60fbe7524a785c66101",
"canvaskit/canvaskit.js": "43fa9e17039a625450b6aba93baf521e",
"canvaskit/canvaskit.wasm": "04ed3c745ff1dee16504be01f9623498",
"index.html": "aa6df4019f5307f96f59056d5ae8e030",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"manifest.json": "57995c0dca1b92e4bdec7931838e63ed"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
